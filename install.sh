#!/bin/bash
ScriptLoc=$(readlink -f "$0")

if [ "$(id -u)" != "0" ];
then
	exec sudo "$0" "$USER" "$@"

	else
		if [ "$1" == "0" ] 
		then
			echo "User should not be root"
			exit 
		fi
fi

TITANUSER=$1
X11CONFIG="/etc/X11/Xwrapper.config"
INTALLPATH="/opt/titan"
DATAPATH="/opt/titan/data"

if [ ! -d "$INTALLPATH" ];
then
	mkdir $INTALLPATH
fi

if [ ! -d "$DATAPATH" ];
then
	mkdir $DATAPATH
fi

echo "Fetching sources..."

# Adding repositories
GECKOBLUEPPA="geckoblu/public"
WEBUP8PPA="webupd8team/java"

if ! grep ^ /etc/apt/sources.list /etc/apt/sources.list.d/* | grep "$GECKOBLUEPPA"; then
	add-apt-repository -y ppa:$GECKOBLUEPPA

	if ! grep ^ /etc/apt/sources.list /etc/apt/sources.list.d/* | grep "$GECKOBLUEPPA"; then
		echo "Problem finding sources. Retrying connection...."
	    exec $ScriptLoc
	    exit
	fi
fi

if ! grep ^ /etc/apt/sources.list /etc/apt/sources.list.d/* | grep "$WEBUP8PPA"; then
	add-apt-repository -y ppa:$WEBUP8PPA

	if ! grep ^ /etc/apt/sources.list /etc/apt/sources.list.d/* | grep "$WEBUP8PPA"; then
		echo "Problem finding sources. Retrying connection...."
	    exec $ScriptLoc
	    exit
	fi
fi

echo "Updating..."
apt-get update

# Install wifi
#apt install network-manager

echo "Installing dependecies..."
# Install display server and audio
if ! type startx > /dev/null;
then
	apt install -y --no-install-recommends xorg openbox pulseaudio
	usermod -a -G audio $TITANUSER

	if ! type startx > /dev/null;
	then
		exec $ScriptLoc
	fi
fi

# Install VLC
if ! type vlc > /dev/null;
then
	apt-get install -y vlc
	if ! type vlc > /dev/null;
	then
		exec $ScriptLoc
	fi
fi
VLCHOMEPATH="VLC_HOME=/usr/lib/vlc"

# Install sqlite
if ! type sqlite > /dev/null;
then
	apt-get install -y sqlite
	if ! type sqlite > /dev/null;
	then
		exec $ScriptLoc
	fi
fi

# Installing Java
if ! type java > /dev/null;
then
	echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
	apt-get -y install oracle-java8-installer
	apt-get -y install oracle-java8-set-default

	if ! type java > /dev/null;
	then
		exec $ScriptLoc
	fi
fi

echo "Configuring..."
# Allow Screen to the user
sed -i '/\(^allowed_users=\).*/ s//\1anybody/' $X11CONFIG

# Installing software

# Titan Jar
JARPATH=${INTALLPATH}/titan.jar

# Titan Script
TITANPATH=${INTALLPATH}/titan.sh
read -r -d '' TITANCONTENT <<EOF
#!/bin/bash

xset -dpms
xset s off
openbox-session &
start-pulseaudio-x11

while true; do
  java -jar $JARPATH
done
EOF

# Titan Init
INITKIOSKPATH="/etc/init/titan.conf"
read -r -d '' INITKIOSKCONTENT <<EOF
start on (filesystem and stopped udevtrigger)
stop on runlevel [06]

console output
emits starting-x

respawn

exec sudo -u $TITANUSER TEMP_DIR=$DATAPATH $VLCHOMEPATH startx $TITANPATH -- -nocursor
EOF

echo "Setting up ground..."

# Installation
echo "$TITANCONTENT" > $TITANPATH
chmod +x $TITANPATH
echo "$INITKIOSKCONTENT" > $INITKIOSKPATH

# Fixing permissions
chown -R $TITANUSER $INTALLPATH
chgrp -R $TITANUSER $INTALLPATH

echo "Final preparations..."
# Configuring boot screen
read -r -d '' GRUBCONFIGCONTENT <<EOF
GRUB_DEFAULT=0
GRUB_HIDDEN_TIMEOUT=0
GRUB_HIDDEN_TIMEOUT_QUIET=true
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
GRUB_CMDLINE_LINUX=""
EOF
echo "$GRUBCONFIGCONTENT" > /etc/default/grub
update-grub

apt-get -y install plymouth-theme-flow

# Reboot
reboot